var currentDate = new Date();
var weekDates = [];
var dayNames = ['Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota', 'Niedziela'];
var dayIDs = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];
var database = firebase.database();
var userID = "";

firebase.auth().onAuthStateChanged(user => {
    if(user) {
        userID = user.uid;
        setWeekDates();
        loadData(currentDate);
    } else {
        window.location = 'index.html';
    }
});

// SET DATA
function setWeekDates() {
    var currentWeekDay = currentDate.getDay();
    if (currentWeekDay == 0) {
        for (var i = 0; i < 6; i++) {
            var date = new Date();
            date.setTime(currentDate.getTime() - (6 - i) * 86400000);
            weekDates.push(date);
        }
        var date = new Date();
        date.setTime(currentDate.getTime());
        weekDates.push(date);
    } else {
        for (var i = 0; i < 7; i++) {
            var date = new Date();
            date.setTime(currentDate.getTime() + (i - currentWeekDay + 1) * 86400000);
            weekDates.push(date);
        }
        var date = new Date();
        date.setTime(currentDate.getTime() + (7 - currentWeekDay) * 86400000);
        weekDates.push(date);
    }
}

function loadData(date) {
    document.getElementById("day-list").value = currentDate.getDate();
    document.getElementById("month-list").value = currentDate.getMonth();
    document.getElementById("year-list").value = currentDate.getFullYear();

    for (var i = 0; i < 7; i++) {
        var date = weekDates[i];
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        fillNote(day, month, year, i);
    }
}

function fillNote(day, month, year, weekDay) {
    var titleString = dayNames[weekDay] + ' ' + day + '/' + month + '/' + year;
    document.getElementById(dayIDs[weekDay]).innerHTML = titleString;
    var note = '';
    firebase.database().ref(userID + '/' + year + '/' + month + '/' + day).once('value').then(function(snapshot) {
        try {
            note = (snapshot.val().note);
        } catch(err) {
            note = "";
        }
        document.getElementById(dayIDs[weekDay] + '-note').value = note;
        if (weekDay == 6) {
            document.getElementById("loader").style.display = "none";
        }
    });
}

// WEEK BUTTONS
function previousWeek() {
    document.getElementById("loader").style.display = "block";
    currentDate.setTime(currentDate.getTime() - 604800000);
    weekDates = [];
    setWeekDates();
    loadData(currentDate);
}

function nextWeek() {
    document.getElementById("loader").style.display = "block";
    currentDate.setTime(currentDate.getTime() + 604800000);
    weekDates = [];
    setWeekDates();
    loadData(currentDate);
}

// SAVE NOTES
function saveNote(day) {
    var note = document.getElementById(dayIDs[day] + '-note').value;
    var year = weekDates[day].getFullYear();
    var month = weekDates[day].getMonth() + 1;
    var day = weekDates[day].getDate();
    database.ref(userID + '/' + year + '/' + month + '/' + day).set({
        note: note
    });
    window.alert('Saved note in ' + day + '/' + month + '/' + year);
}

// LOGOUT
function logout() {
    firebase.auth().signOut().then(function() {
        // LOGOUT
    }).catch(function(error) {
        console.log("ERROR");
    });
}
